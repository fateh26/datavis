import { BigmamaPage } from './app.po';

describe('bigmama App', () => {
  let page: BigmamaPage;

  beforeEach(() => {
    page = new BigmamaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
