import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  @Input () dataChart;

  isbar=false;
  isLine=false;
  isDoughnut=true;
  ChartData:Array<any>;
  ChartLabels:Array<any>;
  ChartOptions:any;
  lineChartColors:Array<any> ;
  lineChartLegend;
  lineChartType;
  barChartType:string;
  barChartLegend:boolean;
  doughnutChartType:string;
  doughnutChartLabels:Array<any>;
  doughnutChartData:Array<any>;


  public chartClicked(e:any):void {
    console.log(e);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }
  constructor() { }

  ngOnInit() {
    /*### prepare data for Doughnut ###*/
    this.getDoughnut();
    
    this.doughnutChartData=this.getYearData(this.dataChart[1][0]);
    this.doughnutChartLabels=['Foreign','Domestic'];
    console.log(this.doughnutChartData);
    console.log(this.doughnutChartLabels);
    /*#### prepare datasets  ####*/
    let dataset=[];
    let foreign=[];
    let domestic=[];
    for ( var i = 1; i < this.dataChart.length; i++) {
        foreign.push(this.dataChart[i][2]); 
        domestic.push(this.dataChart[i][3]);
    }

    this.ChartData= [
      {data: foreign, label: this.dataChart[0][2]},
      {data: domestic, label: this.dataChart[0][3]}
    ];
   
    /*#### prepare labels  ####*/
    let labels=[];
    for ( var i = 1; i < this.dataChart.length; i++) {
      labels.push(String(this.dataChart[i][0])); 
    }
      this.ChartLabels= labels;
    }
   /*### Doughnut chart ###*/
   getDoughnut(){
     this.isDoughnut=true;
     this.isbar=false;
     this.isLine=false;
     this.doughnutChartType="doughnut";
     this.doughnutChartData=this.getYearData(this.dataChart[1][0]);

   }
  getYearData(year:number){
    this.doughnutChartData=[];
    
    for ( var i = 1; i < this.dataChart.length; i++) {
        if (this.dataChart[i][0]===year){

           this.doughnutChartData.push(this.dataChart[i][2],this.dataChart[i][3]);
        }
    }
    return this.doughnutChartData;
  }
  
  getLine(){
      this.isbar=false;
      this.isLine=true;
      this.isDoughnut=false;
  
      this.ChartOptions= {
        responsive: true
      };
     
    this.lineChartLegend = true;
    this.lineChartType = 'line';
   
  }
  getBar(){
    this.isLine=false;
    this.isbar=true;
    this.isDoughnut=false;
    this.ChartOptions= {
      scaleShowVerticalLines: false,
      responsive: true
    };
    this.barChartType= 'bar';
    this.barChartLegend= true; 
  }
  
}
