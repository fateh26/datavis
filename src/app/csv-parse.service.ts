export class CsvParseService {
  preparedData=[[]];

  constructor() { }

  csvtoArray(data:string){
    let elements = data.split(/\r\n|\n/);
    let headers = elements[0].split(/\,|\;/);
    let lines = [];
    lines.push(headers);
    for ( let i = 1; i < elements.length; i++) {
        // split content based on comma
        let cdata = elements[i].split(/\,|\;/);
        if (cdata.length == headers.length) {
            let tarr = [];
            for ( let j = 0; j < headers.length; j++) {
                tarr.push(Number(cdata[j]));
            }
            lines.push(tarr);
        }
    }
    this.preparedData = lines;
    return this.preparedData;

  }

}
