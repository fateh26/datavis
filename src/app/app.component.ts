import { Component } from '@angular/core';
import { CsvParseService } from './csv-parse.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CsvParseService]})
export class AppComponent {
  title = 'app';
  text="";
  preparedData=[[]];
  showVis=false;

  constructor(private  parseService : CsvParseService){}
  
  getContent(Content:{content:string}){
    this.text=Content.content;
    this.showVis=true;
    /*#### calling parse service string content to array[]  ####*/
    this.preparedData=this.parseService.csvtoArray(this.text);
  }
  setSection(section:string){
    if(section === 'importFile'){
      this.showVis=false;
    }  
  }
  
}
