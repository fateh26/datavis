import { Component, OnInit ,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Output() selectAction=new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }
  selectSection(section:string){

   this.selectAction.emit(section);
  }
}
