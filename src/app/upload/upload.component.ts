import { Component, OnInit,ViewChild,ElementRef,Output,EventEmitter } from '@angular/core';
import { ChartsModule } from 'ng2-charts';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  infoMessage='';
  dataString='';
  endmessage="";
  errormessage="";
  loading=false;
  success=false;

  @Output() csvContent=new EventEmitter<{"content":string}>();
  @ViewChild('fileInput') csvFile:ElementRef;

  constructor() {
    
  }
  
  ngOnInit() {
  }
  
  fileUpload() {

      
      let csvfile=this.csvFile.nativeElement.files[0];

      
        var reader = new FileReader();
        reader.onloadstart=()=>{
        this.loading=true;
      }
      reader.onloadend=() => {
        this.loading=false;
       
      }
      reader.onerror=()=>{
         this.endmessage=reader.error.name;
      }
    
      reader.onload=() => {
         this.dataString = reader.result;
         this.success=true;
         this.endmessage="Data successfully imported";

      }
      reader.readAsText(csvfile);
      this.infoMessage= csvfile.name + " - " + csvfile.size/1000+ "kb";
        
  }  

  vis(){
    this.csvContent.emit({content:this.dataString})
  }

  
}
